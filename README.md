# 3 VPN Myths You Should Ignore #

If you have heard of virtual private networks, you would have probably heard several myths about them. Whether or not you believe those myths, we are here to help clear them up. VPNs get an unfair ride and it is time to put the record straight.

Here are the three most common VPN myths:

### They are only used by criminals ###

Yes, some people use a VPN because they want to hide what they are doing online. Whether this is illegally downloading copyrighted content such as movies and music, or doing things much worse that we will not get into, some criminals do use them. However, using a VPN itself is not illegal. You can use them for their true purpose of increasing your online security and privacy. 

### They are scams ###

Again, some of them are scams or are not providing the service that they should be. This is not true for all, however. There are some great VPN providers that have a solid and honest reputation. You just have to find them. This can be done easily by using sites that review privacy software.

### They are expensive ###

This is the silliest of them all. It is not clear why some people believe VPNs to be expensive. They are actually very affordably priced. You can get a month's subscription for the same price you would pay for a sandwich or a hot cup of coffee. On top of that, many offer a free trial or money-back guarantee. 

So, as you can see, a VPN is actually a splendid piece of software that is usually cheap, highly effective at keeping you safe online, and giving you privacy when you browse the web. 

[https://vpnveteran.com/](https://vpnveteran.com/)